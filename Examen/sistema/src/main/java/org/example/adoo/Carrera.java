package org.example.adoo;

public enum Carrera {
    INGENIERIA_COMPUTACION("Ingeniería en Computación"),
    INGENIERIA_SOFTWARE("Ingeniería de Software"),
    INGENIERIA_ELECTRICISTA("Ingeniería en Electricista"),
    INGENIERIA_ELECTRONICA_INDUSTRIAL("Ingeniería en Electrónica Industrial"),
    INGENIERIA_DISENO_INDUSTRIAL("Ingeniería en Diseño Industrial"),
    INGENIERIA_ROBOTICA_MECATRONICA("Ingeniería en Robótica y Mecatrónica");

    private final String descripcion;

    Carrera(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}

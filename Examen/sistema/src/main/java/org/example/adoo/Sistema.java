package org.example.adoo;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Sistema {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcion;
        List<Egresado> egresados = new ArrayList<>();

        do {
            System.out.println("Bienvenido al sistema");
            System.out.println("1. Egresado");
            System.out.println("2. Usuario asignado");
            System.out.println("3. Salir");
            System.out.print("Seleccione una opción: ");

            String opcionStr = scanner.next();
            opcion = Integer.parseInt(opcionStr);

            switch (opcion) {
                case 1:
                    Egresado.añadirEgresado(egresados, scanner);
                    break;
                case 2:
                    UsuarioAsignado.opcionesUsuarioAsignado(egresados, scanner);
                    break;
                case 3:
                    System.out.println("Saliendo del programa...");
                    break;
                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");
            }
        } while (opcion != 3);

        scanner.close();
    }
}

class Egresado {
    private String nombre;
    private String apellidos;
    private String generacion;
    private int matricula;
    private boolean actividadLaboral;
    private String carrera;
    private String correo;

    public Egresado(String nombre, String apellidos, String generacion, int matricula, boolean actividadLaboral, String carrera, String correo) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.generacion = generacion;
        this.matricula = matricula;
        this.actividadLaboral = actividadLaboral;
        this.carrera = carrera;
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getGeneracion() {
        return generacion;
    }

    public void setGeneracion(String generacion) {
        this.generacion = generacion;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public boolean isActividadLaboral() {
        return actividadLaboral;
    }

    public void setActividadLaboral(boolean actividadLaboral) {
        this.actividadLaboral = actividadLaboral;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public static void añadirEgresado(List<Egresado> egresados, Scanner scanner) {
        System.out.println("Ingrese los datos del egresado:");
        System.out.print("Nombre: ");
        String nombre = scanner.next();
        System.out.print("Apellidos: ");
        String apellidos = scanner.next();
        System.out.print("Generación: ");
        String generacion = scanner.next();
        System.out.print("Matrícula: ");
        int matricula = scanner.nextInt();
        System.out.print("Actividad laboral (true/false): ");
        boolean actividadLaboral = scanner.nextBoolean();
        System.out.print("Carrera: ");
        String carrera = scanner.next();
        System.out.print("Correo: ");
        String correo = scanner.next();

        Egresado egresado = new Egresado(nombre, apellidos, generacion, matricula, actividadLaboral, carrera, correo);
        egresados.add(egresado);

        System.out.println("Se envió un correo de confirmación al siguiente correo: " + correo);
    }

    @Override
    public String toString() {
        return "Egresado{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", generacion='" + generacion + '\'' +
                ", matricula=" + matricula +
                ", actividadLaboral=" + actividadLaboral +
                ", carrera='" + carrera + '\'' +
                ", correo='" + correo + '\'' +
                '}';
    }
}

class UsuarioAsignado {
    public static void opcionesUsuarioAsignado(List<Egresado> egresados, Scanner scanner) {
        int opcion;

        do {
            System.out.println("Opciones de Usuario Asignado");
            System.out.println("1. Ver lista de egresados");
            System.out.println("2. Modificar fecha de registro");
            System.out.println("3. Regresar al menú principal");
            System.out.print("Seleccione una opción: ");
            String opcionStr = scanner.next();
            opcion = Integer.parseInt(opcionStr);

            switch (opcion) {
                case 1:
                    verListaEgresados(egresados);
                    break;
                case 2:
                    modificarFechaRegistro(egresados, scanner);
                    break;
                case 3:
                    System.out.println("Regresando al menú principal...");
                    break;
                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");
            }
        } while (opcion != 3);
    }

    private static void verListaEgresados(List<Egresado> egresados) {
        System.out.println("Lista de Egresados:");
        for (Egresado egresado : egresados) {
            System.out.println(egresado.toString());
        }
    }

    private static void modificarFechaRegistro(List<Egresado> egresados, Scanner scanner) {
        System.out.print("Ingrese el correo del egresado para modificar la fecha de registro: ");
        String correo = scanner.next();

        for (Egresado egresado : egresados) {
            if (egresado.getCorreo().equals(correo)) {
                System.out.print("Ingrese la nueva fecha de registro (dd/mm/yyyy): ");
                String nuevaFechaRegistro = scanner.next();
                System.out.println("Fecha cambiada correctamente.");
                return;
            }
        }

        System.out.println("No se encontró ningún egresado con el correo especificado.");
    }
}





